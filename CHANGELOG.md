# Django-Blocklist Changelog

## 2024-10-12

### 2.8.1

* Fixed Django specs that were incorrectly excluding patch versions of 5.1
* Updated import_blocklist options to be more consistent with update_blocklist
* Added totals to report_blocklist table

## 2024-09-14

### 2.8

* Renamed `add_to_blocklist` utils function to `update_blocklist`, adding optional `last_seen` param
* Added missing DB migration for BlockedIP ordering change in v2.7
* Removed unused Django versions from tox.ini

## 2024-09-07

### 2.7

* Updated report_blocklist to sort (descending) by per-reason tally
* Fixed clean_blocklist and admin to deal correctly with never-seen IPs
* Minor change to default ordering of BlockedIP
* Updated settings and urls to support running standalone admin for development
* Clarified/corrected officially supported versions: Python 3.9+, Django 4.2+
* Set up tox and Gitlab CI for all supported Python and Django versions
* Bugfix: Removed typing.Self (requires Python 3.11+)

## 2024-08-08

### 2.6

* Added `--skip-existing` option to `update_blocklist`
* Added summary counts to `update_blocklist` output

## 2024-07-28

### 2.5.1

* Fixed it so last_seen is not set automatically on BlockedIP creation

## 2024-07-04

### 2.5

* Official compatibility with Django 5

## 2024-06-11

### 2.4

* Ensure that cached blocklist is updated when DB changes; changed utils.get_blocklist `force_cache` to `refresh_cache` for clarity

## 2024-04-18

### 2.3.2

* Added `py.typed` so mypy will know to use our type annotations

## 2024-03-03

### v2.3.1

* Fixed an error when an IP is in cache but not DB

## 2024-02-24

### v2.3

* Changed middleware to use cached blocklist, to reduce DB load

## 2024-02-17

### v2.2

* Renamed BlockedIP field `first_seen` to `datetime_added`

## 2024-01-31

### v2.1.2

* Fixed bug in clean_blocklist

## 2024-01-28

### v2.1.1

* Prevent clean_blocklist command error when blocklist is empty. Thanks, @christophehenry!
* Minor improvements to logging in add_to_blocklist
* Development: Added ruff config to pyproject.toml
* Minor fixes to README

## 2023-12-26

### v2.1.0

* Added optional 'cooldown' param to utils.add_to_blocklist
* Improved logging in add_to_blocklist
* Minor improvements/fixes to report_blocklist command
* Minor improvement to fake_blocklist command

## 2023-12-08

### v2.0.0

* Fixed timezone-related problems. Thanks, @ali.salman2!
* Python 3.9 is now the minimum version
* Added mypy and ruff to dev tools

## 2023-07-04

### v1.6.1

* Fixed bug related to settings restrucuring

## 2023-03-19

### v1.6.0

* Moved all default settings values to apps.Config
* clean_blocklist now prints summary when verbosity > 0
* update_blocklist now gives info on which fields were updated

## 2023-02-23

### v1.5.0

* Added "Days left" column to admin list

## 2023-01-25

### v1.4.1

* Minor bugfixes

### v1.4.0

* Added --reason option on report_blocklist

## 2022-12-10

### v1.3.3, v1.3.4

* Minor bugfixes

### v1.3.2

* "Most active" section of report now sorts by, and shows, rate per hour

## 2022-11-20

### v1.3.1

* Middleware now only runs when `settings.DEBUG` is `False`

## 2022-08-08

### v1.3.0

* Replaced `add_to_blocklist` command with expanded `update_blocklist`
* Removed `--last-seen` option from `update_blocklist`

## 2022-08-07

### v1.2.3

* Fixed bug in "Most recent" report section
* Updated sample report and linked from README

## 2022-07-10

### v1.2.2

* Moved to GitLab. No internal changes except updating project URL in config files.

## 2022-07-09

### v1.2.1

* Added requirements files
* Updated project config files to indicate Django 4 compatibility
* Fixed exception handling that caused noise in error-tracking tools
* Minor README edits

## 2022-07-02

### v1.2.0

* Changed default tally to 0
* Added tally to admin list view

## 2022-06-18

### v1.1.0

* Added scaffolding to allow tests to run standalone (outside a project) via `pytest`.
* Updated packaging info to indicate Django 4 compatibility.

## 2022-03-28

### v1.0.3

Minor cleanup of imports.

## 2022-03-18

### v1.0.2

Minor cleanup; expanded README.

## 2022-03-16

### v1.0.1

Initial release.
