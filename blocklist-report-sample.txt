Total blocks of listed IPs: 10,087,074
Entries in blocklist: 12,640
Active in last 24 hours: 1,633
Stale (added over 24h ago, not seen since): 1

Most recent:
192.168.48.164 -- 3,641 blocks in 3 months -- seen a second ago -- 15 day cooldown -- spam
192.168.115.209 -- 2,360 blocks in 2 weeks -- seen 2 seconds ago -- 30 day cooldown -- spam
192.168.148.46 -- 32 blocks in 2 weeks -- seen 2 seconds ago -- 30 day cooldown -- spam
192.168.19.91 -- 1,875,908 blocks in 9 months, 2 weeks -- seen 10 seconds ago -- 30 day cooldown -- ratelimit abuse
192.168.101.27 -- 31 blocks in 1 month -- seen 17 seconds ago -- 7 day cooldown -- firehol

Most active:
192.168.151.74 -- 344,658 blocks in 1 month, 1 week -- seen 2 minutes ago -- 30 day cooldown -- ratelimit abuse -- 368 per hour
192.168.0.2 -- 2,517,438 blocks in 9 months, 2 weeks -- seen 21 seconds ago -- 30 day cooldown -- ratelimit abuse -- 360 per hour
192.168.19.91 -- 1,875,908 blocks in 9 months, 2 weeks -- seen 10 seconds ago -- 30 day cooldown -- ratelimit abuse -- 269 per hour
192.168.66.251 -- 7,821 blocks in 3 days, 18 hours -- seen 2 minutes ago -- 1 day cooldown -- firehol -- 109 per hour
192.168.67.230 -- 208,697 blocks in 2 months, 4 weeks -- seen 10 minutes ago -- 30 day cooldown -- ratelimit abuse -- 96 per hour

Longest lived:
192.168.68.180 -- 1,140,630 blocks in 1 year, 11 months -- seen 13 hours ago -- 30 day cooldown -- ratelimit abuse

Reason                          Seen      Listed    Tally    
------------------------------------------------------------------------
spam                           |   3,262 |   4,373 |   955,958
firehol                        |     608 |     608 | 1,154,244
scraping                       |     322 |     335 |    89,344
ratelimit abuse                |     189 |     409 | 7,706,959
probing                        |      64 |     118 |     7,493
--------------------------------------------------------------
(Totals)                       |   4,445 |   5,843 | 9,913,998
