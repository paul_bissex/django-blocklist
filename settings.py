# Minimal settings, just enough to run the tests and the admin

from typing import Dict, Any

ALLOWED_HOSTS = ["127.0.0.1", "localhost"]
BLOCKLIST_CONFIG: Dict[str, Any] = {}
DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": "db.sqlite3"}}
DEBUG = True
INSTALLED_APPS = [
    "django_blocklist",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.messages",
    "django.contrib.sessions",
]
MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django_blocklist.middleware.BlocklistMiddleware",
]
ROOT_URLCONF = "urls"
SECRET_KEY = 42
STATIC_URL = "https://dpaste.b-cdn.net/static/"  # Replace with your own STATIC_URL if needed
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
            "string_if_invalid": "",
        },
    },
]
USE_TZ = True
