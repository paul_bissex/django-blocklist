"""Just enough of a urls.py to run the tests and see the admin"""

from django.contrib import admin
from django.http import HttpResponse
from django.urls import path

urlpatterns = [
    path("", lambda request: HttpResponse()),
    path("admin/", admin.site.urls),
]
